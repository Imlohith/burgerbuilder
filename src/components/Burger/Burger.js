import React from 'react';
import classes from './Burger.module.css'
import BurgerIngrediant from './BurgerIngrediants/BurgerIngrediants'

const Burger = (props) => {

    let transformedIngredients = Object.keys( props.ingrediants)
    .map( igKey => {
        return [...Array( props.ingrediants[igKey] )].map( ( _, i ) => {
            return <BurgerIngrediant key={igKey + i} type={igKey} />;
        } );
    } )
    .reduce((arr, el) => {
        return arr.concat(el)
    }, []);
    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients!</p>;
    }

    return (
        <div className={classes.Burger}>
             <BurgerIngrediant type="bread-top" />
             { transformedIngredients }
             <BurgerIngrediant type="bread-bottom" />
        </div>    
    );
}
 
export default Burger;