import React from 'react';
import classes from './BuildControls.module.css'

import BuildControl from '../BuildControls/BuildControl/BuildControl'

const controls = [
    { label: 'Cheese', type: 'cheese' },
    { label: 'Salad', type: 'meat' },
    { label: 'Meat', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
]

const BuildControls = (props) => {
    return ( 
        <div className={classes.BuildControls}>
            <p>Trending Burger</p>
            <p>Current price :  <strong>{ props.price.toFixed(2) }</strong> </p>
             { controls.map(ctrl => {
                 return <BuildControl 
                             key={ctrl.label}
                             label={ctrl.label}
                             added={() => props.addIngredaint(ctrl.type)}
                             remove={() => props.removeIngredaint(ctrl.type) }
                             disabled={props.disabled[ctrl.type]} /> 
             }) }
        </div>
     );
}
 
export default BuildControls;