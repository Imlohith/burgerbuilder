import React, { Component } from 'react';

import Aux from '../../hoc/ReactAux'

import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'

const INGREDIANT_PRICES = {
    cheese: 0.5, 
    meat: 1.3,
    salad: 0.7,
    bacon: 0.4
}

class BurgerBuilder extends Component {
    state = {
      Ingrediants: {
          cheese: 0, 
          meat: 0,
          salad: 0,
          bacon: 0
      },
       totalPrice: 4
    }

    addIngrediantHandler = (type) => {

      const oldCount = this.state.Ingrediants[type];
      const updatedCount = oldCount + 1;
      const updatedIngredients = {
          ...this.state.Ingrediants
      };
      updatedIngredients[type] = updatedCount;
      const priceAddition = INGREDIANT_PRICES[type];
      const oldPrice = this.state.totalPrice;
      const newPrice = oldPrice + priceAddition;
      this.setState( { totalPrice: newPrice, Ingrediants: updatedIngredients } );
      console.log(updatedIngredients)
    }

    removeIngrediantHnadler = (type) => {
        const oldCount = this.state.Ingrediants[type];
        if(oldCount <= 0) {
            return;
        }
        const updatedCount = oldCount - 1;
        const updatedIngredients = {
            ...this.state.Ingrediants
        };
        updatedIngredients[type] = updatedCount;
        const priceAddition = INGREDIANT_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceAddition;
        this.setState( { totalPrice: newPrice, Ingrediants: updatedIngredients } );
        console.log(updatedIngredients)
    }
    render() { 
        const disabledInfo = {
            ...this.state.Ingrediants
        }
        for(let key in disabledInfo) {
            disabledInfo[key] = disabledInfo[key] <= 0;
            console.log(disabledInfo[key])
        }
        return ( 
            <Aux>
                <Burger ingrediants={this.state.Ingrediants} />
                <BuildControls 
                           addIngredaint={this.addIngrediantHandler} 
                           removeIngredaint={this.removeIngrediantHnadler}
                           disabled={disabledInfo}
                           price={this.state.totalPrice} />
            </Aux>
         );
    }
}
 
export default BurgerBuilder;